package com.laoding.am_01;

import java.util.Scanner;

public class LoginMenu {
    public void printLoginMenu() {
        int c = 0;
        Scanner input = new Scanner(System.in);
        do {
            System.out.println("\t\t系统登录");
            System.out.println("---------------------------------");
            System.out.print("请输入用户名");
            String uname = input.next();
            System.out.print("请输入密码");
            String upwd = input.next();

            if (uname.equals("admin") && upwd.equals("123456")) {
                c = 0;
                System.out.println("登录成功，请继续");
                break;
            } else {
                c++;
                System.out.println("登录失败！用户名或密码有误！");
                if (c <= 2)
                    System.out.println("密码输入错误" + c + "次，还剩下" + (3 - c) + "次");
                else {
                    System.out.println("非法用户，即将退出");
                }
            }

        } while (c < 3);
    }
}
