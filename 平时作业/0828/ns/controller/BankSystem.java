package com.ns.controller;

import com.ns.pojo.Save;
import com.ns.view.BankSystemMenu;

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class BankSystem {

    HashMap<String, Save> bankdb;//String键：唯一，Save 储户信息
    Scanner input;

    public BankSystem() {//构造函数，作用初始化数据成员
        bankdb = new HashMap<String, Save>();
        //input = new Scanner(String.in);
    }

    public void runBankSystem() throws IOException {

        String flags = "N";
        do {
            BankSystemMenu.showMenu();
            String userInput = input.next();


            switch (userInput) {
                case "1" -> {
                    createAccount();
                }
                case "2" -> {
                    deleteAccount();
                }
                case "3" -> {
                    saveMoney();
                }
                case "4" -> {
                    getMoney();
                }
                case "5" -> {
                    //changeMoney();
                }
                case "6" -> {
                    showBalance();
                }
                case "7" -> {

                }
                default -> {
                    System.out.println("您输入有误！请重试！");
                }
            }
            System.out.println("是否");

        }while (flags.equalsIgnoreCase("y"));
        System.out.println("欢迎下次再来！！！");
    }

    public void createAccount() {
        System.out.println("正在创建账户...");
        System.out.print("请输入用户名:");
        String name=input.next();
        System.out.print("请输入密码:");
        String pwd=input.next();
        System.out.print("请输入性别:");
        String gender=input.next();
        Save save = new Save();
        String accountid="6226"+(int)(1000+(Math.random()*9000));
        System.out.println("已为您生成一个专属卡号："+accountid);
        save.setAccountid(accountid);
        save.setName(name);
        save.setPwd(pwd);
        save.setGender(gender);
        bankdb.put(accountid,save);//银行账号
        System.out.println("您已经开户完成");
        System.out.println("您的开户信息："+bankdb);
    }

    public void showBalance() {
System.out.println("查询余额");
    }
    public void changeMoney(){

    }

    public void saveMoney() {

    }

    public void deleteAccount() {
System.out.println("删除账户");
    }

    public void getMoney() {

    }
    public void exit(){

    }
}
