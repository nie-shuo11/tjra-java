import com.ns.controller.BankSystem;

public class Main {
    public static void main(String[] args) throws Exception {
        BankSystem bankSystem = new BankSystem();
        bankSystem.runBankSystem();
    }
}