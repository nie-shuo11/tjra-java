import java.util.Scanner;

public class LongMenu {
    public void printLoginMenu() {
        Scanner input = new Scanner(System.in);
        int t = 0;
        do {
            System.out.println("\t\t系统登录");
            System.out.println("-----------");
            System.out.println("请输入用户名");
            String name = input.next();
            System.out.println("请输入密码");
            String upwd = input.next();
            if (name.equals("admin") && upwd.equals("123456")) {
                t = 0;
                break;
            } else {
                t++;
                System.out.println("登录失败！您输入的用户名或密码有错误!");
                if (t <= 2) {
                    System.out.println("密码输入错误" + t + "次，还剩下" + (3 - t) + "次");
                } else {

                    System.out.println("非法用户，即将退出系统");
                }
            }


        } while (t < 3);
    }
}
