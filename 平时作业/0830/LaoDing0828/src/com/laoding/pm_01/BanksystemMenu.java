package com.laoding.pm_01;

import java.io.IOException;

public class BanksystemMenu {

    public static void showMenu(){

        System.out.println("24小时银行自助取款系统");
        System.out.println("----------------------------------");
        System.out.println("1.开户");
        System.out.println("2.销户");
        System.out.println("3.存款");
        System.out.println("4.取款");
        System.out.println("5.转账");
        System.out.println("6.查询余额");
        System.out.println("7.退出");
        System.out.println("-----------------------------------");
        System.out.println("请选择：");

    }

    public static void clearScreen() {
        try {
            String os = System.getProperty("os.name");
            if (os.contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
