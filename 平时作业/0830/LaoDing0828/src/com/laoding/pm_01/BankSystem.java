package com.laoding.pm_01;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class BankSystem {
    static int a = 2;
    HashMap<String, Save> bankdb;//String键：唯一，Save 储户信息
    Scanner input = new Scanner(System.in);

    public BankSystem() throws IOException, Exception {//构造函数，作用初始化数据成员
        bankdb = new HashMap<String, Save>();
        initBankDB();
    }

    public void initBankDB() throws IOException, InterruptedException {//读文件，完成bankdb集合的赋值
//文件字符输入流 FileReader BufferedReader
        File file = new File("d:/AccountInfo.txt");//Ctrl+P查看函数参数
        FileReader reader=new FileReader(file);// 低级流
        BufferedReader br=new BufferedReader(reader);//转换为高级流
        String str;
        while ((str=br.readLine())!=null){
            String[]save_arr=  str.split(",");
            Save save=new Save();
            save.setAccount_id(save_arr[0]);
            save.setName(save_arr[1]);
            save.setPwd(save_arr[2]);
            save.setGender(save_arr[3]);
            save.setMoney(Double.parseDouble(save_arr[4]));//字符串转double
            bankdb.put(save_arr[0],save);
            Thread.sleep(100);
            System.out.println(save);
        }
        System.out.println("账户初始化信息完毕！");
    }

    public void runBankSystem() throws Exception {
        String c = "n";
        do {
            BanksystemMenu.showMenu();
            String userInput = input.next();
            switch (userInput) {
                case "1" -> {
                    createAccount();
                }
                case "2" -> {
                    deleteAccount();
                }
                case "3" -> {
                    saveMoney();
                }
                case "4" -> {
                    getMoney();
                }
                case "5" -> {
                    changeMoney();
                }
                case "6" -> {
                    showBalance();
                }
                case "7" -> {
                    exit();
                }
                default -> {
                    System.out.println("输入有误,请重试！");
                    continue;
                }
            }
            if (a == 1) {
                continue;
            }
            if (a == 0) {
                c = "y";
                continue;
            }
            System.out.println("是否继续使用本系统（y/n）");
            c = input.next();
        } while (c.equalsIgnoreCase("y"));
    }

    //功能代码
//开户
    public void createAccount() throws Exception {
        System.out.println("正在创建账户...");
        Thread.sleep(1000);
        System.out.println("-----------------");
        System.out.print("请输入用户名:");
        String name = input.next();
        System.out.print("请输入密码:");
        String pwd = input.next();
        System.out.print("请输入性别:");
        String gender = input.next();
        Save save = new Save();
        String accountid = "6226" + (int) (1000 + (Math.random() * 9000));
        System.out.println("已为您生成一个专属卡号：" + accountid);
        save.setAccount_id(accountid);
        save.setName(name);
        save.setPwd(pwd);
        save.setGender(gender);
        bankdb.put(accountid, save);//银行账号
        System.out.println("您已经开户完成");
        Thread.sleep(1000);
        System.out.println("您的开户信息：" + bankdb);
    }

    //销户
    public void deleteAccount() throws Exception {
        System.out.println("-----------------");
        System.out.println("请输入您的银行卡号：");
        boolean iscon = false;
        boolean iscon1 = false;
        String account = input.next();
        if (bankdb.containsKey(account)) {
            Save save = bankdb.get(account);
            int count = 3;
            do {
                System.out.println("请输入密码：");
                String pwd = input.next();
                if (pwd.equals(save.getPwd())) {
                    Thread.sleep(1000);
                    bankdb.remove(account);
                    System.out.println("销户完成");
                    iscon = false;
                } else {
                    if (count <= 0) {
                        System.out.println("机会用尽，请滚。");
                        return;
                    }
                    count--;
                    System.out.println("您输入的密码有误，还有" + count + "次机会，请重试。");
                    iscon = true;
                }
            } while (iscon);
        } else {
            System.out.println("您输入的账户有误。");
            iscon1 = true;
            while (iscon1) {
                deleteAccount();
                iscon1 = false;
            }
        }
    }

    //查询
    public void showBalance() throws Exception {
        System.out.println("正在查询余额...");
        Thread.sleep(1000);
        System.out.println("-----------------");
        System.out.println("请输入您的银行卡号：");
        boolean iscon = false;
        boolean iscon1 = false;
        String account = input.next();
        if (bankdb.containsKey(account)) {
            Save save = bankdb.get(account);
            int count = 3;
            do {
                System.out.println("请输入密码：");
                String pwd = input.next();
                if (pwd.equals(save.getPwd())) {
                    System.out.println("正在查询账户信息...");
                    Thread.sleep(1000);
                    bankdb.remove(account);
                    System.err.printf("您的卡号：%s，您的姓名：%s，您的余额：%f\n", save.getAccount_id(), save.getName(), save.getMoney());
                    iscon = false;
                } else {
                    if (count <= 0) {
                        System.out.println("机会用尽，请滚。");
                        return;
                    }
                    count--;
                    System.out.println("您输入的密码有误，还有" + count + "次机会，请重试。");
                    iscon = true;
                }
            } while (iscon);
        } else {
            System.out.println("您输入的账户有误。");
            iscon1 = true;
            while (iscon1) {
                showBalance();
                iscon1 = false;
            }
        }

    }

    //存款
    public void saveMoney() throws Exception {
        System.out.println("正在进行存款...");
        Thread.sleep(1000);
        System.out.println("-----------------");
        System.out.println("请输入您的银行卡号：");
        boolean iscon = false;
        boolean iscon1 = false;
        String account = input.next();
        if (bankdb.containsKey(account)) {
            Save save = bankdb.get(account);
            int count = 3;
            do {
                System.out.println("请输入密码：");
                String pwd = input.next();
                if (pwd.equals(save.getPwd())) {
                    System.out.println("请输入存款金额：");
                    double money = input.nextDouble();
                    save.setMoney(money+save.getMoney());
                    bankdb.put(account,save);
                    Thread.sleep(1000);
                    System.out.println("存款成功。");
                    System.err.printf("您的卡号：%s，您的姓名：%s，您的余额：%f\n", save.getAccount_id(), save.getName(), save.getMoney());
                    iscon = false;
                } else {
                    if (count <= 0) {
                        System.out.println("机会用尽，请滚。");
                        return;
                    }
                    count--;
                    System.out.println("您输入的密码有误，还有" + count + "次机会，请重试。");
                    iscon = true;
                }
            } while (iscon);
        } else {
            System.out.println("您输入的账户有误。");
            iscon1 = true;
            while (iscon1) {
                saveMoney();
                iscon1 = false;
            }
        }
    }

    //取款
    public void getMoney1(String account1) throws Exception {
        Save save = bankdb.get(account1);
        double money = input.nextDouble();
        if (money <= save.getMoney()){
            save.setMoney(save.getMoney()-money);
            bankdb.put(account1,save);
            Thread.sleep(1000);
            System.out.println("取款成功，请拿走您的取款。");
            System.err.printf("您的卡号：%s，您的姓名：%s，您的余额：%f\n", save.getAccount_id(), save.getName(), save.getMoney());
        }
        else {
            System.out.println("您的余额不足，请重新输入取款金额。");
            getMoney1(account1);
        }
    }

    public void getMoney() throws Exception {
        System.out.println("正在进行取款...");
        Thread.sleep(1000);
        System.out.println("-----------------");
        System.out.println("请输入您的银行卡号：");
        boolean iscon = false;
        boolean iscon1 = false;
        String account = input.next();
        if (bankdb.containsKey(account)) {
            Save save = bankdb.get(account);
            int count = 3;
            do {
                System.out.println("请输入密码：");
                String pwd = input.next();
                if (pwd.equals(save.getPwd())) {
                    System.out.println("请输入取款金额：");
                    double money = input.nextDouble();
                    if (money <= save.getMoney()){
                        save.setMoney(save.getMoney()-money);
                        bankdb.put(account,save);
                        Thread.sleep(1000);
                        System.out.println("取款成功，请拿走您的取款。");
                        System.err.printf("您的卡号：%s，您的姓名：%s，您的余额：%f\n", save.getAccount_id(), save.getName(), save.getMoney());
                    }
                    else {
                        System.out.println("您的余额不足，请重新输入取款金额：");
                        getMoney1(account);
                    }
                    iscon = false;
                } else {
                    if (count <= 0) {
                        System.out.println("机会用尽，请滚。");
                        return;
                    }
                    count--;
                    System.out.println("您输入的密码有误，还有" + count + "次机会，请重试。");
                    iscon = true;
                }
            } while (iscon);
        } else {
            System.out.println("您输入的账户有误。");
            iscon1 = true;
            while (iscon1) {
                getMoney();
                iscon1 = false;
            }
        }
    }

    //转账
    public void changeMoney() {
        System.out.println("转账功能失效。");
    }

    //退出
    public void exit() {
        System.out.println("是否退出？（y|n）");
        String userIN = input.next();
        if (userIN.equalsIgnoreCase("y")) {
            a = 1;
            System.exit(0);
        } else {
            a = 0;
        }
    }
}
