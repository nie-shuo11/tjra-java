package control;

import pojo.Save;
import view.Back.BackSystemMenu;

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class BackSystem {
    HashMap<String, Save> bankdb;
    Scanner input;

    public BackSystem() {
        bankdb = new HashMap<>();
        input = new Scanner(System.in);;
    }

    public void runBankSystem() throws IOException {
        String flags = "N";
        do {
            BackSystemMenu.showMenu();
            String userInput = input.next();
            switch (userInput) {
                case "1" -> { //不用 Break，jdk14 以上
                    createAccount();//ctrl+鼠标左键
                }
                case "2" -> {
                    deleteAccount();
                }
                case "3" -> {
                    saveMoney();
                }
                case "4" -> {
                    getMoney();
                }
                case "5" -> {
                    changeMoney();
                }
                case "6" -> {
                    showBalance();
                }
                case "7" -> {
                    exit();
                }
                default -> {
                    System.out.println("您输入有误！请重试！");
                }
            }
            System.out.println("是否继续操作本系统?(Y|N)");
            flags = input.next();
            BackSystemMenu.clearScreen();//清屏
        } while (flags.equalsIgnoreCase("y"));
        System.out.println("欢迎下次再来！！");
    }
    //开户
    public void createAccount() {
        System.out.println("正在创建账户...");
        System.out.print("请输入用户名:");
        String name=input.next();
        System.out.print("请输入密码:");
        String pwd=input.next();
        System.out.print("请输入性别:");
        String gender=input.next();
        Save save = new Save();
        String accountid="6226"+(int)(1000+(Math.random()*9000));
        System.out.println("已为您生成一个专属卡号："+accountid);
        save.setAccountid(accountid);
        save.setName(name);
        save.setPwd(pwd);
        save.setGender(gender);
        bankdb.put(accountid,save);//银行账号
        System.out.println("您已经开户完成");
        System.out.println("您的开户信息："+bankdb);
    }
    //存款
    public void saveMoney() throws Exception {
        boolean iscontinue =           false;
        int count = 3;
        System.out.println("正在执行存钱功能...");
        System.out.println("----------------");
        System.out.print("请输入您的银行账号：");
        String account = input.next();
        if (bankdb.containsKey(account)) {
            Save save = bankdb.get(account);
            do {
                System.out.print("请输入您的密码：");
                String pwd = input.next();
                if (pwd.equals(save.getPwd())) {
                    System.out.print("请输入存款金额：");
                    double money=input.nextDouble();
                    save.setMoney(money+save.getMoney());
                    bankdb.put(account,save);
                    System.out.println("存款成功！");
                    System.err.printf("您的卡号：%s,您的姓名:%s,您的余额:%f\n", save.getAccountid(), save.getName(),
                    save.getMoney());
                    iscontinue = false;
                } else {
                    //可以添加还有几次机会
                    count--;
                    if (count == 0) {
                        System.out.println("您的密码次数已经用完，即将退出查询功能");
                                Thread.sleep(1000);
                        return;
                    }
                    System.out.println("您的密码输入有误,还剩下" +count + "次机会，请重试!");
                    iscontinue = true;
                }
            } while (iscontinue);
        } else {
            System.out.println("您的银行号输入有误！");

        }
    }
    //取款
    public void getMoney() {
    }
    //转账
    public void changeMoney() {
    }
    public void showBalance() throws Exception {
        boolean iscontinue = false;
        int count = 3;
        System.out.println("正在执行查询余额...");
        System.out.println("-------------------");
        System.out.print("请输入您的银行账号:");
        String account = input.next();
        if (bankdb.containsKey(account)) {
            Save save = bankdb.get(account);
            do {
                System.out.print("请输入您的密码：");
                String pwd = input.next();
                if (pwd.equals(save.getPwd())) {
                    System.out.println("正在查询账户信息，请稍后...");
                    Thread.sleep(1000);
                    System.err.printf("您的卡号：%s,您的姓名:%s,您的余" + "额:%f\n", save.getAccountid(), save.getName(),
                            save.getMoney());
                    iscontinue = false;
                } else {
                    //可以添加还有几次机会
                    count--;
                    if (count == 0) {
                        System.out.println("您的密码次数已经用完，即将退出查询功能");
                        Thread.sleep(1000);
                        return;
                    }
                    System.out.println("您的密码输入有误,还剩下" + count + "次机会，请重试!");
                    iscontinue = true;
                }
            } while (iscontinue);
        }else {
            System.out.println("您的银行卡号输入有误");
        }
    }
    //销户
    public void deleteAccount() throws Exception {
        boolean iscontinue = false;
        int count = 3;
        System.out.println("正在执行销户...");
        System.out.println("------------------");
        System.out.print("请输入您的银行账号:");
        String account = input.next();
        if (bankdb.containsKey(account)) {
            Save save = bankdb.get(account);
            do {
                System.out.print("请输入您的密码：");
                String pwd = input.next();
                if (pwd.equals(save.getPwd())) {
                    System.out.println("正在销户，请稍后...");
                    Thread.sleep(1000);
                    bankdb.remove(account);
                    System.out.println("销户完成！");
                    iscontinue = false;
                } else {
                    //可以添加还有几次机会
                    count--;
                    if (count == 0) {
                        System.out.println("您的密码次数已经用完，即将退出销户功能");
                                Thread.sleep(1000);
                        return;
                    }
                    System.out.println("您的密码输入有误,还剩下" + count + "次机会，请重试!");
                    iscontinue = true;
                }
            } while (iscontinue);
        } else {
            System.out.println("您的银行号输入有误！");
            //可以重复几次
        }
    }

    //退出系统
    public void exit() {
        System.out.print("是否真的要退出系统？（Y|N）:");
        String userInput = input.next();
        if (userInput.equalsIgnoreCase("y")) {
            System.exit(0);
        }
    }
}