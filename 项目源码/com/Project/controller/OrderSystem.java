package com.Project.controller;

import com.Project.View.OrderSystemMenu;
import com.Project.dao.impl.LoginSystemDao;
import com.Project.dao.impl.MenuDao;
import com.Project.dao.impl.OrderSystemDao;
import com.Project.object.User;
import com.Project.object.Order;

import java.io.IOException;
import java.util.Scanner;

public class OrderSystem {
    Scanner sc = new Scanner(System.in);
    public OrderSystem() throws IOException, Exception {//构造函数，作用初始化数据成员

    }

    public void login() throws Exception {
        boolean flag1 = true;
        while (flag1) {
            OrderSystemMenu.showMenu1();
            int a = sc.nextInt();
            int count = 0;
            if (a == 1) {
                while (true) {
                    System.out.println("请输入账号:");
                    String account = sc.next();
                    System.out.print("请输入名称:");
                    String name = sc.next();
                    LoginSystemDao loginSystem = new LoginSystemDao();
                    User user = loginSystem.findByAccount(account);
                    if(user == null){
                        System.out.println("查无此人。");
                        break;
                    }
                    System.out.println("请输入密码:");
                    String loginpwd = sc.next();
                    boolean flag = false;
                    if (account.equals(user.getAccount()) && loginpwd.equals(user.getLoginpwd())) {
                        System.out.println("登录成功");
                        flag = true;
                    }
                    else {
                        System.out.println("登录失败");
                    }

                    if (flag) {
                        order();
                        return;
                    } else {
                        count++;
                        if (count == 3) {
                            flag1 = false;
                            break;
                        }
                        System.out.println("账号或密码有误,还有" + (3 - count) + "次机会输入");
                    }
                }
            } else if (a == 2) {
                System.out.println("正在创建账户...");
                Thread.sleep(1000);
                System.out.println("-----------------");
                String account = "108934" + (int) (1000 + (Math.random() * 9000));
                System.out.println("已为您生成一个专属账户：" + account);
                System.out.print("请输入密码:");
                String loginpwd = sc.next();
                System.out.print("请输入名称:");
                String name = sc.next();
                User user = new User();
                user.setAccount(account);
                user.setLoginpwd(loginpwd);
                user.setName(name);
                LoginSystemDao loginSystem = new LoginSystemDao();
                int f = loginSystem.insert(user);
                if (f > 0) {
                    System.out.println("您已经创建账户完成");
                    Thread.sleep(1000);
                    System.out.println("您的账号信息：" + user);
                    System.out.println("请重新登录");
                } else {
                    System.out.println("创建失败");
                }

            } else if (a == 3) {
                System.exit(0);
            } else {
                System.out.println("输入有误,请重新输入");
            }
        }
    }

    public void order() {
        while (true) {
            OrderSystemMenu.showMenu();
            int input = sc.nextInt();
            if (input == 4) {
                System.out.println("是否退出？（y|n）");
                String userIN = sc.next();
                if (userIN.equalsIgnoreCase("y")) {
                    System.exit(0);
                }
            }

            switch (input) {
                case 1:
                    ergodic();
                    break;
                case 2:
                    ordermenu();
                    break;
                case 3:
                    Query_Order();
                    break;
                case 4:
                    System.exit(0);
                    break;
                default:
                    System.out.print("输入有误,请重新输入：");
                    break;
            }
        }
    }

    public void ergodic() {
        for (int id =1;;id++){
            OrderSystemDao orderSystem = new OrderSystemDao();
            Order order = orderSystem.findById(id);
            if (order == null) {
                return;
            }else {
                System.out.println(order);
            }
        }
    }

    double sum = 0.0;
    public void ordermenu() {
        System.out.println("请输入菜品名称下单" + "\t\t(输入exit退出点单)");
        String name = sc.next();
        OrderSystemDao orderSystem = new OrderSystemDao();
        Order order = orderSystem.findByName(name);
        if (order != null) {

        } else {
            System.out.println("查无此菜！");
        }
        boolean p = true;
        while (p){
            if (name.equals(order.getName())){
                Order order1 = new Order();
                order1.setName(name);
                order1.setPrice(order.getPrice());
                order1.setId(order.getId());
                MenuDao menu1 = new MenuDao();
                int f = menu1.insert(order1);
                if (f > 0) {
                    sum += order1.getPrice();
                    System.out.println("添加完成");
                } else {
                    System.out.println("添加失败");
                }
                break;
            }
            if (name.equals("exit")) {
                p = false;
            }
        }

//        System.out.println("总价格:" + sum);
    }

    public void Query_Order() {

        System.out.println("总价格:" + sum);
//        System.out.println(order.toString());
    }

}
