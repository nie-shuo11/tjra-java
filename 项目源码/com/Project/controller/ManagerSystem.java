package com.Project.controller;


import com.Project.View.ManagerSystemMenu;
import com.Project.dao.impl.ManagerSystemDao;
import com.Project.dao.impl.OrderSystemDao;
import com.Project.object.Order;
import com.Project.object.Manager;
import com.Project.service.impl.IManagerSystemService;
import com.Project.service.impl.ManagerSystemService;

import java.io.IOException;
import java.util.Scanner;

public class ManagerSystem {
    static int a = 2;
    static int b = 2;
    Scanner input = new Scanner(System.in);
    IManagerSystemService iManagerSystemService;
    public ManagerSystem() throws IOException, Exception {
            iManagerSystemService = new ManagerSystemService();
    }

    public void runSavaSystem() throws Exception {
        String c = "n";
        do {
            if (b == 1) {
                ManagerSystemMenu.showMenu1();
                String userInput = input.next();
                switch (userInput) {
                    case "1" -> {
//                        System.out.println("暂无");
                        ergodic();
                    }
                    case "2" -> {
                        add();
                    }
                    case "3" -> {
                        reduce();
                    }
                    case "4" -> {
                        modify();
                    }
                    case "5" -> {
                        showMenu();
                    }
                    case "6" -> {
                        System.out.println("暂无");
                    }
                    case "7" -> {
                        b = 2;
                    }
                    default -> {
                        System.out.println("输入有误,请重试！");
                        continue;
                    }
                }
            } else {
                ManagerSystemMenu.showMenu();
                String userInput = input.next();
                switch (userInput) {
                    case "1" -> {
                        createAccount();
                    }
                    case "2" -> {
                        deleteAccount();
                    }
                    case "3" -> {
                        login();
                    }
                    case "4" -> {
                        exit();
                    }
                    default -> {
                        System.out.println("输入有误,请重试！");
                        continue;
                    }
                }
            }
            if (a == 1) {
                continue;
            }
            if (a == 0) {
                c = "y";
                continue;
            }
            System.out.println("是否继续使用本系统（y/n）");
            c = input.next();
        } while (c.equalsIgnoreCase("y"));
    }


    //      功能代码
    //  开户
    public void createAccount() throws Exception {
        System.out.println("正在创建账户...");
        Thread.sleep(1000);
        System.out.println("-----------------");
        System.out.print("请输入用户名:");
        String name = input.next();
        System.out.print("请输入密码:");
        String pwd = input.next();
        System.out.print("请输入性别:");
        String sex = input.next();
        Manager manager = new Manager();
        String accountid = "108934" + (int) (1000 + (Math.random() * 9000));
        System.out.println("已为您生成一个专属账户：" + accountid);
        manager.setName(name);
        manager.setAccount(accountid);
        manager.setLoginpwd(pwd);
        manager.setSex(sex);
//        导入数据库
        ManagerSystemDao managerSystem = new ManagerSystemDao();
        int f = managerSystem.insert(manager);
        if (f > 0) {
            System.out.println("您已经创建账户完成");
        } else {
            System.out.println("创建失败");
        }
        Thread.sleep(100);
        System.out.println("您的账号信息：" + manager);

    }

    //  销户
    public void deleteAccount() throws Exception {
        boolean iscon = false;
        boolean iscon1 = false;
        System.out.println("-----------------");
        System.out.print("请输入您的账号：");
        String account = input.next();
        System.out.print("请输入您的名称：");
        String name = input.next();
        ManagerSystemDao managerSystem = new ManagerSystemDao();
        Manager manager1 = managerSystem.findByName(name);
        if (manager1 != null) {

        } else {
            System.out.println("查无此人！");
            return;
        }

        if (account.equals(manager1.getAccount())) {
            int count = 3;
            do {
                System.out.print("请输入密码：");
                String pwd = input.next();
                Manager manager = managerSystem.findByName(name);
                if (pwd.equals(manager.getLoginpwd())) {
                    Thread.sleep(100);
                    int f = managerSystem.deleteByName(name);
                    if (f > 0) {
                        System.out.println("您已经销户完成");
                    } else {
                        System.out.println("销户失败");
                    }
                    iscon = false;
                } else {
                    if (count <= 0) {
                        System.out.println("机会用尽，系统关闭。");
                        return;
                    }
                    count--;
                    System.out.println("您输入的密码有误，还有" + count + "次机会，请重试。");
                    iscon = true;
                }
            } while (iscon);
        } else {
            System.out.println("您输入的账户有误。");
            iscon1 = true;
            while (iscon1) {
                deleteAccount();
                iscon1 = false;
            }
        }

    }

    //  登录
    public void login() {
        System.out.print("请输入账号：");
        String id = input.next();
        System.out.print("请输入名称：");
        String name = input.next();
        ManagerSystemDao managerSystem = new ManagerSystemDao();
        Manager manager = managerSystem.findByName(name);
        if (manager != null) {

        } else {
            System.out.println("查无此人！");
            return;
        }
        boolean flag1 = true;
        while (flag1) {
            int count = 0;
            while (true) {
                System.out.print("请输入密码：");
                String password = input.next();
                boolean flag = false;
                if (password.equals(manager.getLoginpwd())) {
                    flag = true;
                    System.out.println("登入成功");
                }
                if (flag) {
                    b = 1;
                    return;
                } else {
                    count++;
                    if (count == 3) {
                        flag1 = false;
                        break;
                    }
                    System.out.println("账号或密码有误,还有" + (3 - count) + "次机会输入");
                }
            }
        }
    }

    //  查询菜单
    public void ergodic() {
        for (int id =1;;id++){
            OrderSystemDao orderSystem = new OrderSystemDao();
            Order order = orderSystem.findById(id);
            if (order == null) {
                return;
            }else {
                System.out.println(order);
            }
        }
    }

    //  加入菜单
    public void add() {
        Order order = new Order();
        System.out.print("请输入添加菜品编号：");
        int id = input.nextInt();
        System.out.print("请输入添加菜品名称：");
        String name = input.next();
        System.out.print("请输入菜品价格：");
        double price = input.nextDouble();
        order.setName(name);
        order.setPrice(price);
        order.setId(id);
        OrderSystemDao orderSystem = new OrderSystemDao();
        int f = orderSystem.insert(order);
        if (f > 0) {
            System.out.println("录入菜品成功\n" + order.toString());

        } else {
            System.out.println("录入菜品失败");
        }
    }

    //  删除菜单
    public void reduce() {
        boolean flag = true;
        while (flag) {
            System.out.print("请输入要删除的菜品名称：");
            String name = input.next();
            System.out.print("请输入要删除的菜品编号：");
            int id = input.nextInt();
            OrderSystemDao orderSystem = new OrderSystemDao();
            Order order = orderSystem.findByName(name);
            if (order != null) {

            } else {
                System.out.println("查无此菜品！");
                return;
            }
            if (name.equals(order.getName())) {
                int f = orderSystem.deleteByName(name);
                if (f > 0) {
                    System.out.println("您已经删除完成");
                } else {
                    System.out.println("删除失败");
                }
                flag = false;
            }
            if (flag) {
                System.out.println("查无此菜品,请重新输入。");
            }
        }

    }

    //  修改菜单价格
    public void modify() {
        boolean flag = true;
        while (flag) {
            System.out.print("请输入要修改价格的菜品名称与编号：");
            String name = input.next();
            int id = input.nextInt();
            OrderSystemDao orderSystem = new OrderSystemDao();
            Order order = orderSystem.findByName(name);
            if (order != null) {

            } else {
                System.out.println("查无此菜品！");
                return;
            }
            if (name.equals(order.getName())) {
                System.out.print("请输入要修改的价格：");
                double price = input.nextDouble();
                order.setPrice(price);
                order.setName(name);
                order.setId(id);
//                导入数据库
                int f = orderSystem.updateByName(order);
                if (f > 0) {
                    System.out.println("您已经修改完成");
                } else {
                    System.out.println("修改失败");
                }
                flag = false;
            }
            if (flag) {
                System.out.print("查无此菜品,请重新输入：");
            }
        }
    }

    //  查询菜品
    public void showMenu() {
        System.out.print("请输入菜品名称：");
        String name = input.next();
        OrderSystemDao orderSystem = new OrderSystemDao();
        Order order = orderSystem.findByName(name);
        if (order != null) {
            System.out.println(order);
        } else {
            System.out.println("查无此菜！");
        }
    }

    //  退出
    public void exit() {
        System.out.println("是否退出？（y|n）");
        String userIN = input.next();
        if (userIN.equalsIgnoreCase("y")) {
            a = 1;
            System.exit(0);
        } else {
            a = 0;
        }
    }


}
