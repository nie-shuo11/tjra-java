package com.Project.object;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class Order {

    private int id;
    private String name;
    private double price;

    @Override
    public String toString(){
        return getId()+"\t\t菜品:"+getName()+"\t\t价格:"+getPrice();
    }

}
