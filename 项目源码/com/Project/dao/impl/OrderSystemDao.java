package com.Project.dao.impl;

import com.Project.object.Order;
import com.Project.utils.DBUtils;

import java.util.ArrayList;
import java.util.List;

public class OrderSystemDao implements IOrderSystemImpl {
    @Override
    public int insert(Order person) {
        //核心代码
        String sql = "insert into menu (Id,Name,Price) values (?,?,?)";
        List<Object> list = new ArrayList();
        list.add(person.getId());
        list.add(person.getName());
        list.add(person.getPrice());
        int i = DBUtils.executeUpdate(sql, list);
        return i;
    }

    @Override
    public Order findByName(String name) {
        String sql = "select * from menu where Name=?";
        List list = new ArrayList();
        list.add(name);
        List<Order> list1 = DBUtils.executeQuery(sql, list, Order.class);
        if (list1.size()==0){
            return null;
        }
        return list1.get(0);
    }


    @Override
    public Order findById(int id) {
        String sql = "select * from menu where Id=?";
        List list = new ArrayList();
        list.add(id);
        List<Order> list1 = DBUtils.executeQuery(sql, list, Order.class);
        if (list1.size()==0){
            return null;
        }
        return list1.get(0);
    }

    @Override
    public int deleteByName(String name) {
        String sql = "delete from menu where Name=?";
        List<Object> list = new ArrayList();
        list.add(name);
        int i = DBUtils.executeUpdate(sql, list);
        return i;
    }

    @Override
    public int updateByName(Order person){
        String sql ="update menu set Id=?,Name=?,Price=? where Name=?";
        List<Object> list = new ArrayList();
        list.add(person.getId());
        list.add(person.getName());
        list.add(person.getPrice());
        list.add(person.getName());
        int i = DBUtils.executeUpdate(sql, list);
        return i;
    }

}
