package com.Project.dao.impl;

import com.Project.object.Order;

public interface IOrderSystemImpl {
    int insert(Order person);

    Order findByName(String name);

    Order findById(int id);

    int deleteByName(String name);

    int updateByName(Order person);
}
