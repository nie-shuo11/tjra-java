package com.Project.dao.impl;

import com.Project.object.User;

public interface ILoginSystemImpl {
    int insert(User person);

    User findByAccount(String account);

    User findByName(String name);
}
