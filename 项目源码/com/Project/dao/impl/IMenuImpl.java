package com.Project.dao.impl;

import com.Project.object.Order;

public interface IMenuImpl {
    int insert(Order person);

    Order findByName(String name);
}
