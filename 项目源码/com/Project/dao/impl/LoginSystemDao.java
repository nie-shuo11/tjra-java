package com.Project.dao.impl;

import com.Project.object.User;
import com.Project.utils.DBUtils;

import java.util.ArrayList;
import java.util.List;

public class LoginSystemDao implements ILoginSystemImpl {
    @Override
    public int insert(User person) {
        //核心代码
        String sql = "insert into user (Name,LoginPwd,Account) values (?,?,?)";
        List<Object> list = new ArrayList();
        list.add(person.getName());
        list.add(person.getLoginpwd());
        list.add(person.getAccount());
        int i = DBUtils.executeUpdate(sql, list);
        return i;
    }

    @Override
    public User findByAccount(String account) {
        String sql = "select * from user where Account=?";
        List list = new ArrayList();
        list.add(account);
        List<User> list1 = DBUtils.executeQuery(sql, list, User.class);
        if (list1.size()==0){
            return null;
        }
        return list1.get(0);
    }

    @Override
    public User findByName(String name) {
        String sql = "select * from user where Name=?";
        List list = new ArrayList();
        list.add(name);
        List<User> list1 = DBUtils.executeQuery(sql, list, User.class);
        if (list1.size()==0){
            return null;
        }
        return list1.get(0);
    }
}
