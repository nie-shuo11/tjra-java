package com.Project.dao.impl;

import com.Project.object.Manager;
import com.Project.utils.DBUtils;

import java.util.ArrayList;
import java.util.List;

public class ManagerSystemDao implements IManagerSystemImpl {
    @Override
    public int insert(Manager person) {
        //核心代码
        String sql = "insert into manager (Name,Account,LoginPwd,Sex) values (?,?,?,?)";
        List<Object> list = new ArrayList<>();
        list.add(person.getName());
        list.add(person.getAccount());
        list.add(person.getLoginpwd());
        list.add(person.getSex());
        int i = DBUtils.executeUpdate(sql, list);
        return i;
    }

    @Override
    public Manager findByName(String name) {
        String sql = "select * from manager where Name=?";
        List list = new ArrayList();
        list.add(name);
        List<Manager> list1 = DBUtils.executeQuery(sql, list, Manager.class);
        if (list1.size()==0){
            return null;
        }
        return list1.get(0);
    }

    @Override
    public Manager findByAccount(String account) {
        String sql = "select * from manager where Account=?";
        List list = new ArrayList();
        list.add(account);
        List<Manager> list1 = DBUtils.executeQuery(sql, list, Manager.class);
        if (list1.size()==0){
            return null;
        }
        return list1.get(0);
    }

    @Override
    public int deleteByName(String name) {
        String sql = "delete from manager where Name=?";
        List<Object> list = new ArrayList();
        list.add(name);
        int i = DBUtils.executeUpdate(sql, list);
        return i;
    }

    @Override
    public int updateByName(Manager person){
        String sql ="update manager set Name=?,Account=?,LoginPwd=?,Sex=?";
        List<Object> list = new ArrayList<>();
        list.add(person.getName());
        list.add(person.getAccount());
        list.add(person.getLoginpwd());
        list.add(person.getSex());
        int i = DBUtils.executeUpdate(sql, list);
        return i;
    }

}
