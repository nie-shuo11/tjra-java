package com.Project.dao.impl;

import com.Project.object.Manager;

public interface IManagerSystemImpl {
    int insert(Manager person);

    Manager findByName(String name);

    Manager findByAccount(String account);

    int deleteByName(String name);

    int updateByName(Manager person);
}
