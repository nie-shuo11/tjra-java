package com.Project.dao.impl;

import com.Project.object.Order;
import com.Project.utils.DBUtils;

import java.util.ArrayList;
import java.util.List;

public class MenuDao implements IMenuImpl {
    @Override
    public int insert(Order person) {
        //核心代码
        String sql = "insert into ordermenu (Id,Name,Price) values (?,?,?)";
        List<Object> list = new ArrayList();
        list.add(person.getId());
        list.add(person.getName());
        list.add(person.getPrice());
        int i = DBUtils.executeUpdate(sql, list);
        return i;
    }

    @Override
    public Order findByName(String name) {
        String sql = "select * from ordermenu where Name=?";
        List list = new ArrayList();
        list.add(name);
        List<Order> list1 = DBUtils.executeQuery(sql, list, Order.class);
        if (list1.size()==0){
            return null;
        }
        return list1.get(0);
    }

}
