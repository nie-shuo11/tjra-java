package com.Project.service.impl;

import com.Project.object.Manager;

public interface IManagerSystemService {
    int insert(Manager person);

    Manager findByName(String name);

    int deleteByName(String name);

    int updateByName(Manager person);
}
