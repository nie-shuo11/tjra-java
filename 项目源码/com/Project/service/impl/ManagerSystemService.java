package com.Project.service.impl;

import com.Project.dao.impl.IManagerSystemImpl;
import com.Project.dao.impl.ManagerSystemDao;
import com.Project.object.Manager;

public class ManagerSystemService implements IManagerSystemService {
    IManagerSystemImpl iManagerSystem;

    public ManagerSystemService(){
        iManagerSystem = new ManagerSystemDao();
    }

    public Manager findByAccount(String account){
        return iManagerSystem.findByAccount(account);
    }

    @Override
    public int insert(Manager person) {
        int i = iManagerSystem.insert(person);
        return i;
    }

    @Override
    public Manager findByName(String name) {
        return iManagerSystem.findByName(name);
    }

    @Override
    public int deleteByName(String name) {
        return iManagerSystem.deleteByName(name);
    }

    @Override
    public int updateByName(Manager person) {
        return iManagerSystem.updateByName(person);
    }

}
